﻿namespace LR4_Queueing_System
{
    partial class MainForm
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.numUD_queue_capasity = new System.Windows.Forms.NumericUpDown();
            this.groupBox_capasity = new System.Windows.Forms.GroupBox();
            this.label_queue_capasity = new System.Windows.Forms.Label();
            this.label_version = new System.Windows.Forms.Label();
            this.groupBox_outer = new System.Windows.Forms.GroupBox();
            this.label_interval_generate_max = new System.Windows.Forms.Label();
            this.numUD_interval_generate_max = new System.Windows.Forms.NumericUpDown();
            this.label_interval_generate_min = new System.Windows.Forms.Label();
            this.label_interval_generate = new System.Windows.Forms.Label();
            this.numUD_interval_generate_min = new System.Windows.Forms.NumericUpDown();
            this.textBox_outer_log = new System.Windows.Forms.TextBox();
            this.button_start = new System.Windows.Forms.Button();
            this.button_stop = new System.Windows.Forms.Button();
            this.groupBox_device = new System.Windows.Forms.GroupBox();
            this.label_nDevices = new System.Windows.Forms.Label();
            this.numUD_nDevices = new System.Windows.Forms.NumericUpDown();
            this.label_processing_scatter = new System.Windows.Forms.Label();
            this.numUD_processing_scatter = new System.Windows.Forms.NumericUpDown();
            this.label_processing_average = new System.Windows.Forms.Label();
            this.label_interval_processing = new System.Windows.Forms.Label();
            this.numUD_processing_average = new System.Windows.Forms.NumericUpDown();
            this.textBox_device_log = new System.Windows.Forms.TextBox();
            this.checkBox_clear = new System.Windows.Forms.CheckBox();
            this.label_stat_percent_fails = new System.Windows.Forms.Label();
            this.textBox_stat_percent_fails = new System.Windows.Forms.TextBox();
            this.textBox_stat_busy = new System.Windows.Forms.TextBox();
            this.label_stat_busy = new System.Windows.Forms.Label();
            this.textBox_stat_n_allrequests = new System.Windows.Forms.TextBox();
            this.label_stat_n_allrequests = new System.Windows.Forms.Label();
            this.textBox_stat_n_fails = new System.Windows.Forms.TextBox();
            this.label_stat_n_fails = new System.Windows.Forms.Label();
            this.groupBox_stats = new System.Windows.Forms.GroupBox();
            this.label_queue_load = new System.Windows.Forms.Label();
            this.textBox_queue_load = new System.Windows.Forms.TextBox();
            this.label_n_reqInQueue = new System.Windows.Forms.Label();
            this.textBox_n_reqInQueue = new System.Windows.Forms.TextBox();
            this.checkBox_disable_logs = new System.Windows.Forms.CheckBox();
            ((System.ComponentModel.ISupportInitialize)(this.numUD_queue_capasity)).BeginInit();
            this.groupBox_capasity.SuspendLayout();
            this.groupBox_outer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numUD_interval_generate_max)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numUD_interval_generate_min)).BeginInit();
            this.groupBox_device.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numUD_nDevices)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numUD_processing_scatter)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numUD_processing_average)).BeginInit();
            this.groupBox_stats.SuspendLayout();
            this.SuspendLayout();
            // 
            // numUD_queue_capasity
            // 
            this.numUD_queue_capasity.Location = new System.Drawing.Point(97, 19);
            this.numUD_queue_capasity.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.numUD_queue_capasity.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numUD_queue_capasity.Name = "numUD_queue_capasity";
            this.numUD_queue_capasity.Size = new System.Drawing.Size(53, 20);
            this.numUD_queue_capasity.TabIndex = 0;
            this.numUD_queue_capasity.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
            // 
            // groupBox_capasity
            // 
            this.groupBox_capasity.Controls.Add(this.label_queue_capasity);
            this.groupBox_capasity.Controls.Add(this.numUD_queue_capasity);
            this.groupBox_capasity.ForeColor = System.Drawing.Color.DarkRed;
            this.groupBox_capasity.Location = new System.Drawing.Point(12, 3);
            this.groupBox_capasity.Name = "groupBox_capasity";
            this.groupBox_capasity.Size = new System.Drawing.Size(311, 63);
            this.groupBox_capasity.TabIndex = 1;
            this.groupBox_capasity.TabStop = false;
            this.groupBox_capasity.Text = "Параметры очереди";
            // 
            // label_queue_capasity
            // 
            this.label_queue_capasity.AutoSize = true;
            this.label_queue_capasity.ForeColor = System.Drawing.Color.Black;
            this.label_queue_capasity.Location = new System.Drawing.Point(7, 21);
            this.label_queue_capasity.Name = "label_queue_capasity";
            this.label_queue_capasity.Size = new System.Drawing.Size(84, 13);
            this.label_queue_capasity.TabIndex = 2;
            this.label_queue_capasity.Text = "Длина очереди";
            // 
            // label_version
            // 
            this.label_version.AutoSize = true;
            this.label_version.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label_version.ForeColor = System.Drawing.Color.DarkGray;
            this.label_version.Location = new System.Drawing.Point(620, -1);
            this.label_version.Name = "label_version";
            this.label_version.Size = new System.Drawing.Size(31, 13);
            this.label_version.TabIndex = 2;
            this.label_version.Text = "v.#.#";
            // 
            // groupBox_outer
            // 
            this.groupBox_outer.Controls.Add(this.label_interval_generate_max);
            this.groupBox_outer.Controls.Add(this.numUD_interval_generate_max);
            this.groupBox_outer.Controls.Add(this.label_interval_generate_min);
            this.groupBox_outer.Controls.Add(this.label_interval_generate);
            this.groupBox_outer.Controls.Add(this.numUD_interval_generate_min);
            this.groupBox_outer.ForeColor = System.Drawing.Color.DarkRed;
            this.groupBox_outer.Location = new System.Drawing.Point(12, 69);
            this.groupBox_outer.Name = "groupBox_outer";
            this.groupBox_outer.Size = new System.Drawing.Size(311, 91);
            this.groupBox_outer.TabIndex = 3;
            this.groupBox_outer.TabStop = false;
            this.groupBox_outer.Text = "Параметры внешней среды";
            // 
            // label_interval_generate_max
            // 
            this.label_interval_generate_max.AutoSize = true;
            this.label_interval_generate_max.ForeColor = System.Drawing.Color.Black;
            this.label_interval_generate_max.Location = new System.Drawing.Point(7, 61);
            this.label_interval_generate_max.Name = "label_interval_generate_max";
            this.label_interval_generate_max.Size = new System.Drawing.Size(117, 13);
            this.label_interval_generate_max.TabIndex = 5;
            this.label_interval_generate_max.Text = "верхний предел (сек):";
            // 
            // numUD_interval_generate_max
            // 
            this.numUD_interval_generate_max.DecimalPlaces = 3;
            this.numUD_interval_generate_max.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.numUD_interval_generate_max.Location = new System.Drawing.Point(125, 59);
            this.numUD_interval_generate_max.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.numUD_interval_generate_max.Minimum = new decimal(new int[] {
            5,
            0,
            0,
            196608});
            this.numUD_interval_generate_max.Name = "numUD_interval_generate_max";
            this.numUD_interval_generate_max.Size = new System.Drawing.Size(64, 20);
            this.numUD_interval_generate_max.TabIndex = 4;
            this.numUD_interval_generate_max.Value = new decimal(new int[] {
            15,
            0,
            0,
            65536});
            // 
            // label_interval_generate_min
            // 
            this.label_interval_generate_min.AutoSize = true;
            this.label_interval_generate_min.ForeColor = System.Drawing.Color.Black;
            this.label_interval_generate_min.Location = new System.Drawing.Point(10, 39);
            this.label_interval_generate_min.Name = "label_interval_generate_min";
            this.label_interval_generate_min.Size = new System.Drawing.Size(114, 13);
            this.label_interval_generate_min.TabIndex = 3;
            this.label_interval_generate_min.Text = "нижний предел (сек):";
            // 
            // label_interval_generate
            // 
            this.label_interval_generate.AutoSize = true;
            this.label_interval_generate.ForeColor = System.Drawing.Color.Black;
            this.label_interval_generate.Location = new System.Drawing.Point(7, 21);
            this.label_interval_generate.Name = "label_interval_generate";
            this.label_interval_generate.Size = new System.Drawing.Size(212, 13);
            this.label_interval_generate.TabIndex = 2;
            this.label_interval_generate.Text = "Интервал времени поступления заявки:";
            // 
            // numUD_interval_generate_min
            // 
            this.numUD_interval_generate_min.DecimalPlaces = 3;
            this.numUD_interval_generate_min.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.numUD_interval_generate_min.Location = new System.Drawing.Point(125, 37);
            this.numUD_interval_generate_min.Maximum = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.numUD_interval_generate_min.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            196608});
            this.numUD_interval_generate_min.Name = "numUD_interval_generate_min";
            this.numUD_interval_generate_min.Size = new System.Drawing.Size(64, 20);
            this.numUD_interval_generate_min.TabIndex = 0;
            this.numUD_interval_generate_min.Value = new decimal(new int[] {
            5,
            0,
            0,
            65536});
            // 
            // textBox_outer_log
            // 
            this.textBox_outer_log.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.textBox_outer_log.Location = new System.Drawing.Point(12, 168);
            this.textBox_outer_log.Multiline = true;
            this.textBox_outer_log.Name = "textBox_outer_log";
            this.textBox_outer_log.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.textBox_outer_log.Size = new System.Drawing.Size(91, 273);
            this.textBox_outer_log.TabIndex = 4;
            // 
            // button_start
            // 
            this.button_start.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button_start.ForeColor = System.Drawing.Color.DarkGreen;
            this.button_start.Location = new System.Drawing.Point(361, 8);
            this.button_start.Name = "button_start";
            this.button_start.Size = new System.Drawing.Size(117, 29);
            this.button_start.TabIndex = 5;
            this.button_start.Text = "  Запустить";
            this.button_start.UseVisualStyleBackColor = true;
            this.button_start.Click += new System.EventHandler(this.button_start_Click);
            // 
            // button_stop
            // 
            this.button_stop.Enabled = false;
            this.button_stop.ForeColor = System.Drawing.Color.Red;
            this.button_stop.Location = new System.Drawing.Point(493, 8);
            this.button_stop.Name = "button_stop";
            this.button_stop.Size = new System.Drawing.Size(117, 29);
            this.button_stop.TabIndex = 6;
            this.button_stop.Text = "Остановить";
            this.button_stop.UseVisualStyleBackColor = true;
            this.button_stop.Click += new System.EventHandler(this.button_stop_Click);
            // 
            // groupBox_device
            // 
            this.groupBox_device.Controls.Add(this.label_nDevices);
            this.groupBox_device.Controls.Add(this.numUD_nDevices);
            this.groupBox_device.Controls.Add(this.label_processing_scatter);
            this.groupBox_device.Controls.Add(this.numUD_processing_scatter);
            this.groupBox_device.Controls.Add(this.label_processing_average);
            this.groupBox_device.Controls.Add(this.label_interval_processing);
            this.groupBox_device.Controls.Add(this.numUD_processing_average);
            this.groupBox_device.ForeColor = System.Drawing.Color.DarkRed;
            this.groupBox_device.Location = new System.Drawing.Point(329, 48);
            this.groupBox_device.Name = "groupBox_device";
            this.groupBox_device.Size = new System.Drawing.Size(311, 112);
            this.groupBox_device.TabIndex = 6;
            this.groupBox_device.TabStop = false;
            this.groupBox_device.Text = "Параметры обслуживающего устройства";
            // 
            // label_nDevices
            // 
            this.label_nDevices.AutoSize = true;
            this.label_nDevices.ForeColor = System.Drawing.Color.Black;
            this.label_nDevices.Location = new System.Drawing.Point(7, 89);
            this.label_nDevices.Name = "label_nDevices";
            this.label_nDevices.Size = new System.Drawing.Size(188, 13);
            this.label_nDevices.TabIndex = 7;
            this.label_nDevices.Text = "количество работающих устройств:";
            // 
            // numUD_nDevices
            // 
            this.numUD_nDevices.Location = new System.Drawing.Point(195, 87);
            this.numUD_nDevices.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numUD_nDevices.Name = "numUD_nDevices";
            this.numUD_nDevices.Size = new System.Drawing.Size(64, 20);
            this.numUD_nDevices.TabIndex = 6;
            this.numUD_nDevices.Value = new decimal(new int[] {
            5,
            0,
            0,
            0});
            // 
            // label_processing_scatter
            // 
            this.label_processing_scatter.AutoSize = true;
            this.label_processing_scatter.ForeColor = System.Drawing.Color.Black;
            this.label_processing_scatter.Location = new System.Drawing.Point(125, 61);
            this.label_processing_scatter.Name = "label_processing_scatter";
            this.label_processing_scatter.Size = new System.Drawing.Size(52, 13);
            this.label_processing_scatter.TabIndex = 5;
            this.label_processing_scatter.Text = "разброс:";
            // 
            // numUD_processing_scatter
            // 
            this.numUD_processing_scatter.DecimalPlaces = 3;
            this.numUD_processing_scatter.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.numUD_processing_scatter.Location = new System.Drawing.Point(181, 59);
            this.numUD_processing_scatter.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.numUD_processing_scatter.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            196608});
            this.numUD_processing_scatter.Name = "numUD_processing_scatter";
            this.numUD_processing_scatter.Size = new System.Drawing.Size(64, 20);
            this.numUD_processing_scatter.TabIndex = 4;
            this.numUD_processing_scatter.Value = new decimal(new int[] {
            3,
            0,
            0,
            65536});
            // 
            // label_processing_average
            // 
            this.label_processing_average.AutoSize = true;
            this.label_processing_average.ForeColor = System.Drawing.Color.Black;
            this.label_processing_average.Location = new System.Drawing.Point(7, 39);
            this.label_processing_average.Name = "label_processing_average";
            this.label_processing_average.Size = new System.Drawing.Size(170, 13);
            this.label_processing_average.TabIndex = 3;
            this.label_processing_average.Text = "среднее время обработки (сек):";
            // 
            // label_interval_processing
            // 
            this.label_interval_processing.AutoSize = true;
            this.label_interval_processing.ForeColor = System.Drawing.Color.Black;
            this.label_interval_processing.Location = new System.Drawing.Point(7, 21);
            this.label_interval_processing.Name = "label_interval_processing";
            this.label_interval_processing.Size = new System.Drawing.Size(201, 13);
            this.label_interval_processing.TabIndex = 2;
            this.label_interval_processing.Text = "Интервал времени обработки заявки:";
            // 
            // numUD_processing_average
            // 
            this.numUD_processing_average.DecimalPlaces = 3;
            this.numUD_processing_average.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.numUD_processing_average.Location = new System.Drawing.Point(181, 37);
            this.numUD_processing_average.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.numUD_processing_average.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            196608});
            this.numUD_processing_average.Name = "numUD_processing_average";
            this.numUD_processing_average.Size = new System.Drawing.Size(64, 20);
            this.numUD_processing_average.TabIndex = 0;
            this.numUD_processing_average.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // textBox_device_log
            // 
            this.textBox_device_log.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.textBox_device_log.Location = new System.Drawing.Point(493, 168);
            this.textBox_device_log.Multiline = true;
            this.textBox_device_log.Name = "textBox_device_log";
            this.textBox_device_log.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.textBox_device_log.Size = new System.Drawing.Size(147, 273);
            this.textBox_device_log.TabIndex = 7;
            // 
            // checkBox_clear
            // 
            this.checkBox_clear.AutoSize = true;
            this.checkBox_clear.Location = new System.Drawing.Point(369, 15);
            this.checkBox_clear.Name = "checkBox_clear";
            this.checkBox_clear.Size = new System.Drawing.Size(15, 14);
            this.checkBox_clear.TabIndex = 8;
            this.checkBox_clear.UseVisualStyleBackColor = true;
            // 
            // label_stat_percent_fails
            // 
            this.label_stat_percent_fails.AutoSize = true;
            this.label_stat_percent_fails.ForeColor = System.Drawing.Color.Black;
            this.label_stat_percent_fails.Location = new System.Drawing.Point(22, 59);
            this.label_stat_percent_fails.Name = "label_stat_percent_fails";
            this.label_stat_percent_fails.Size = new System.Drawing.Size(162, 13);
            this.label_stat_percent_fails.TabIndex = 9;
            this.label_stat_percent_fails.Text = "процент пропущенных заявок:";
            // 
            // textBox_stat_percent_fails
            // 
            this.textBox_stat_percent_fails.BackColor = System.Drawing.Color.White;
            this.textBox_stat_percent_fails.Location = new System.Drawing.Point(202, 56);
            this.textBox_stat_percent_fails.Name = "textBox_stat_percent_fails";
            this.textBox_stat_percent_fails.ReadOnly = true;
            this.textBox_stat_percent_fails.Size = new System.Drawing.Size(42, 20);
            this.textBox_stat_percent_fails.TabIndex = 10;
            // 
            // textBox_stat_busy
            // 
            this.textBox_stat_busy.BackColor = System.Drawing.Color.White;
            this.textBox_stat_busy.Location = new System.Drawing.Point(190, 146);
            this.textBox_stat_busy.Name = "textBox_stat_busy";
            this.textBox_stat_busy.ReadOnly = true;
            this.textBox_stat_busy.Size = new System.Drawing.Size(42, 20);
            this.textBox_stat_busy.TabIndex = 12;
            // 
            // label_stat_busy
            // 
            this.label_stat_busy.AutoSize = true;
            this.label_stat_busy.ForeColor = System.Drawing.Color.Black;
            this.label_stat_busy.Location = new System.Drawing.Point(22, 149);
            this.label_stat_busy.Name = "label_stat_busy";
            this.label_stat_busy.Size = new System.Drawing.Size(167, 13);
            this.label_stat_busy.TabIndex = 11;
            this.label_stat_busy.Text = "количество занятых устройств:";
            // 
            // textBox_stat_n_allrequests
            // 
            this.textBox_stat_n_allrequests.BackColor = System.Drawing.Color.White;
            this.textBox_stat_n_allrequests.Location = new System.Drawing.Point(202, 33);
            this.textBox_stat_n_allrequests.Name = "textBox_stat_n_allrequests";
            this.textBox_stat_n_allrequests.ReadOnly = true;
            this.textBox_stat_n_allrequests.Size = new System.Drawing.Size(42, 20);
            this.textBox_stat_n_allrequests.TabIndex = 18;
            // 
            // label_stat_n_allrequests
            // 
            this.label_stat_n_allrequests.AutoSize = true;
            this.label_stat_n_allrequests.ForeColor = System.Drawing.Color.Black;
            this.label_stat_n_allrequests.Location = new System.Drawing.Point(22, 36);
            this.label_stat_n_allrequests.Name = "label_stat_n_allrequests";
            this.label_stat_n_allrequests.Size = new System.Drawing.Size(78, 13);
            this.label_stat_n_allrequests.TabIndex = 17;
            this.label_stat_n_allrequests.Text = "всего заявок:";
            // 
            // textBox_stat_n_fails
            // 
            this.textBox_stat_n_fails.BackColor = System.Drawing.Color.White;
            this.textBox_stat_n_fails.Location = new System.Drawing.Point(202, 13);
            this.textBox_stat_n_fails.Name = "textBox_stat_n_fails";
            this.textBox_stat_n_fails.ReadOnly = true;
            this.textBox_stat_n_fails.Size = new System.Drawing.Size(42, 20);
            this.textBox_stat_n_fails.TabIndex = 16;
            // 
            // label_stat_n_fails
            // 
            this.label_stat_n_fails.AutoSize = true;
            this.label_stat_n_fails.ForeColor = System.Drawing.Color.Black;
            this.label_stat_n_fails.Location = new System.Drawing.Point(22, 16);
            this.label_stat_n_fails.Name = "label_stat_n_fails";
            this.label_stat_n_fails.Size = new System.Drawing.Size(179, 13);
            this.label_stat_n_fails.TabIndex = 15;
            this.label_stat_n_fails.Text = "количество пропущенных заявок:";
            // 
            // groupBox_stats
            // 
            this.groupBox_stats.Controls.Add(this.label_queue_load);
            this.groupBox_stats.Controls.Add(this.textBox_queue_load);
            this.groupBox_stats.Controls.Add(this.label_n_reqInQueue);
            this.groupBox_stats.Controls.Add(this.textBox_n_reqInQueue);
            this.groupBox_stats.Controls.Add(this.textBox_stat_n_allrequests);
            this.groupBox_stats.Controls.Add(this.label_stat_n_fails);
            this.groupBox_stats.Controls.Add(this.label_stat_n_allrequests);
            this.groupBox_stats.Controls.Add(this.label_stat_percent_fails);
            this.groupBox_stats.Controls.Add(this.textBox_stat_n_fails);
            this.groupBox_stats.Controls.Add(this.textBox_stat_percent_fails);
            this.groupBox_stats.Controls.Add(this.label_stat_busy);
            this.groupBox_stats.Controls.Add(this.textBox_stat_busy);
            this.groupBox_stats.ForeColor = System.Drawing.Color.DarkRed;
            this.groupBox_stats.Location = new System.Drawing.Point(109, 162);
            this.groupBox_stats.Name = "groupBox_stats";
            this.groupBox_stats.Size = new System.Drawing.Size(378, 181);
            this.groupBox_stats.TabIndex = 19;
            this.groupBox_stats.TabStop = false;
            this.groupBox_stats.Text = "Статистика";
            // 
            // label_queue_load
            // 
            this.label_queue_load.AutoSize = true;
            this.label_queue_load.ForeColor = System.Drawing.Color.Black;
            this.label_queue_load.Location = new System.Drawing.Point(22, 118);
            this.label_queue_load.Name = "label_queue_load";
            this.label_queue_load.Size = new System.Drawing.Size(131, 13);
            this.label_queue_load.TabIndex = 21;
            this.label_queue_load.Text = "загруженность очереди:";
            // 
            // textBox_queue_load
            // 
            this.textBox_queue_load.BackColor = System.Drawing.Color.White;
            this.textBox_queue_load.Location = new System.Drawing.Point(190, 115);
            this.textBox_queue_load.Name = "textBox_queue_load";
            this.textBox_queue_load.ReadOnly = true;
            this.textBox_queue_load.Size = new System.Drawing.Size(50, 20);
            this.textBox_queue_load.TabIndex = 22;
            // 
            // label_n_reqInQueue
            // 
            this.label_n_reqInQueue.AutoSize = true;
            this.label_n_reqInQueue.ForeColor = System.Drawing.Color.Black;
            this.label_n_reqInQueue.Location = new System.Drawing.Point(22, 98);
            this.label_n_reqInQueue.Name = "label_n_reqInQueue";
            this.label_n_reqInQueue.Size = new System.Drawing.Size(99, 13);
            this.label_n_reqInQueue.TabIndex = 19;
            this.label_n_reqInQueue.Text = "заявок в очереди:";
            // 
            // textBox_n_reqInQueue
            // 
            this.textBox_n_reqInQueue.BackColor = System.Drawing.Color.White;
            this.textBox_n_reqInQueue.Location = new System.Drawing.Point(190, 95);
            this.textBox_n_reqInQueue.Name = "textBox_n_reqInQueue";
            this.textBox_n_reqInQueue.ReadOnly = true;
            this.textBox_n_reqInQueue.Size = new System.Drawing.Size(50, 20);
            this.textBox_n_reqInQueue.TabIndex = 20;
            // 
            // checkBox_disable_logs
            // 
            this.checkBox_disable_logs.AutoSize = true;
            this.checkBox_disable_logs.Location = new System.Drawing.Point(347, 33);
            this.checkBox_disable_logs.Name = "checkBox_disable_logs";
            this.checkBox_disable_logs.Size = new System.Drawing.Size(130, 17);
            this.checkBox_disable_logs.TabIndex = 20;
            this.checkBox_disable_logs.Text = "запустить без логов";
            this.checkBox_disable_logs.UseVisualStyleBackColor = true;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(650, 462);
            this.Controls.Add(this.checkBox_clear);
            this.Controls.Add(this.textBox_device_log);
            this.Controls.Add(this.groupBox_device);
            this.Controls.Add(this.button_stop);
            this.Controls.Add(this.button_start);
            this.Controls.Add(this.textBox_outer_log);
            this.Controls.Add(this.groupBox_outer);
            this.Controls.Add(this.label_version);
            this.Controls.Add(this.groupBox_capasity);
            this.Controls.Add(this.groupBox_stats);
            this.Controls.Add(this.checkBox_disable_logs);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(666, 500);
            this.Name = "MainForm";
            this.Text = "ЛР 4 :: Queueing System";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainForm_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.numUD_queue_capasity)).EndInit();
            this.groupBox_capasity.ResumeLayout(false);
            this.groupBox_capasity.PerformLayout();
            this.groupBox_outer.ResumeLayout(false);
            this.groupBox_outer.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numUD_interval_generate_max)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numUD_interval_generate_min)).EndInit();
            this.groupBox_device.ResumeLayout(false);
            this.groupBox_device.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numUD_nDevices)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numUD_processing_scatter)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numUD_processing_average)).EndInit();
            this.groupBox_stats.ResumeLayout(false);
            this.groupBox_stats.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox_capasity;
        private System.Windows.Forms.Label label_queue_capasity;
        /// <summary>селектор длины очереди</summary>
        private System.Windows.Forms.NumericUpDown numUD_queue_capasity;
        private System.Windows.Forms.Label label_version;
        private System.Windows.Forms.GroupBox groupBox_outer;
        private System.Windows.Forms.Label label_interval_generate;
        private System.Windows.Forms.Label label_interval_generate_min;
        private System.Windows.Forms.Label label_interval_generate_max;
        /// <summary>нижний предел (сек) времени поступления заявки</summary>
        private System.Windows.Forms.NumericUpDown numUD_interval_generate_min;
        /// <summary>верхний предел (сек) времени поступления заявки</summary>
        private System.Windows.Forms.NumericUpDown numUD_interval_generate_max;
        private System.Windows.Forms.TextBox textBox_outer_log;
        /// <summary>кнопка запуска системы</summary>
        private System.Windows.Forms.Button button_start;
        /// <summary>кнопка уничтожения системы</summary>
        private System.Windows.Forms.Button button_stop;
        private System.Windows.Forms.GroupBox groupBox_device;
        private System.Windows.Forms.Label label_interval_processing;
        private System.Windows.Forms.Label label_processing_average;
        private System.Windows.Forms.Label label_processing_scatter;
        /// <summary>среднее время обработки заявки (сек)</summary>
        private System.Windows.Forms.NumericUpDown numUD_processing_average;
        /// <summary>разброс времени обработки заявки (сек)</summary>
        private System.Windows.Forms.NumericUpDown numUD_processing_scatter;
        private System.Windows.Forms.TextBox textBox_device_log;
        /// <summary>Очищать ли логи при запуске</summary>
        private System.Windows.Forms.CheckBox checkBox_clear;
        private System.Windows.Forms.Label label_stat_percent_fails;
        private System.Windows.Forms.TextBox textBox_stat_percent_fails;
        private System.Windows.Forms.TextBox textBox_stat_busy;
        private System.Windows.Forms.Label label_stat_busy;
        private System.Windows.Forms.TextBox textBox_stat_n_allrequests;
        private System.Windows.Forms.Label label_stat_n_allrequests;
        private System.Windows.Forms.TextBox textBox_stat_n_fails;
        private System.Windows.Forms.Label label_stat_n_fails;
        private System.Windows.Forms.GroupBox groupBox_stats;
        private System.Windows.Forms.Label label_nDevices;
        private System.Windows.Forms.NumericUpDown numUD_nDevices;
        private System.Windows.Forms.Label label_n_reqInQueue;
        private System.Windows.Forms.TextBox textBox_n_reqInQueue;
        private System.Windows.Forms.Label label_queue_load;
        private System.Windows.Forms.TextBox textBox_queue_load;
        private System.Windows.Forms.CheckBox checkBox_disable_logs;
    }
}

