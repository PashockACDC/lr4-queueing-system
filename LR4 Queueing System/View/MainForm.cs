﻿using LR4_Queueing_System.Controller;
using LR4_Queueing_System.ViewInterfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LR4_Queueing_System
{
    public partial class MainForm : Form, IMainView
    {
        private string version = "v.0.5";
        private MainController mController;

        public MainForm()
        {
            InitializeComponent();
            this.label_version.Text = version;
            this.mController = new MainController(this);
            (new ToolTip()).SetToolTip(label_version, "Старцев П. С. группа ВИУ7-71");
            (new ToolTip()).SetToolTip(checkBox_clear, "Очищать логи при запуске");
            (new ToolTip()).SetToolTip(label_processing_average, "Или μ - мат. ожидание");
            (new ToolTip()).SetToolTip(numUD_processing_average, "Или μ - мат. ожидание");
            (new ToolTip()).SetToolTip(label_processing_scatter, "Или σ - средневкадратическое отклонение");
            (new ToolTip()).SetToolTip(numUD_processing_scatter, "Или σ - средневкадратическое отклонение");
        }

        /// <summary>
        /// TEST TEST TEST TEST TEST TEST TEST TEST
        /// </summary>
        /// <param name="message"></param>
        /// <param name="outer">если true, то outer</param>
        void IMainView.addMessageToLog(string message, bool outer = true)
        {
            try
            {
                this.Invoke((Action)(() =>
                {
                    if (!checkBox_disable_logs.Checked)
                    {
                        if (outer)
                        {
                            StringBuilder str = new StringBuilder(textBox_outer_log.Text);
                            str.Append(message).Append("\r\n");
                            textBox_outer_log.Text = str.ToString();
                            textBox_outer_log.SelectionStart = textBox_outer_log.TextLength;
                            textBox_outer_log.ScrollToCaret();
                        }
                        else
                        {
                            StringBuilder str = new StringBuilder(textBox_device_log.Text);
                            str.Append(message).Append("\r\n");
                            textBox_device_log.Text = str.ToString();
                            textBox_device_log.SelectionStart = textBox_device_log.TextLength;
                            textBox_device_log.ScrollToCaret();
                        }
                    }
                }));
            }
            catch (Exception ex)
            { /* не знаю как иначе обработать ошибку "Доступ к ликвидированному объекту невозможен" */ }
        }

        /// <summary>
        /// Показываем статистику
        /// </summary>
        /// <param name="nFails">кол-во пропущенных заявок</param>
        /// <param name="nBusy">кол-во занятых устройств</param>
        /// <param name="nAllDevices">всего устройств</param>
        /// <param name="nAllReq">всего заявок поступило</param>
        /// <param name="nReqInQueue">заявок в очереди</param>
        void IMainView.showStatictic(int nFails, int nBusy, int nAllDevices, int nAllReq, int nReqInQueue)
        {
            try
            {
                this.Invoke((Action)(() =>
                {
                    textBox_stat_n_fails.Text = nFails.ToString();
                    textBox_stat_n_allrequests.Text = nAllReq.ToString();
                    double percentFails = 0;
                    if (nAllReq != 0)
                        percentFails = ((double)nFails / nAllReq) * 100;
                    textBox_stat_percent_fails.Text = percentFails.ToString("F2") + "%";
                    textBox_stat_busy.Text = nBusy.ToString();
                    textBox_n_reqInQueue.Text = nReqInQueue.ToString();
                    double percentQueueLoad = ((double)nReqInQueue / (int)numUD_queue_capasity.Value) * 100;
                    textBox_queue_load.Text = percentQueueLoad.ToString("F2") + "%";
                }));
            }
            catch (Exception ex)
            { /* не знаю как иначе обработать ошибку "Доступ к ликвидированному объекту невозможен" */ }
        }

        private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            mController.destroy();
        }

        private void button_start_Click(object sender, EventArgs e)
        {
            if (checkBox_clear.Checked)
            {
                textBox_device_log.Text = "";
                textBox_outer_log.Text = "";
            }
            button_start.Enabled = false;
            groupBox_capasity.Enabled = false;
            groupBox_outer.Enabled = false;
            groupBox_device.Enabled = false;
            button_stop.Enabled = true;
            this.mController.createSystem((double)numUD_interval_generate_min.Value,
                                          (double)numUD_interval_generate_max.Value,
                                          (double)numUD_processing_average.Value,
                                          (double)numUD_processing_scatter.Value,
                                          (int)numUD_queue_capasity.Value,
                                          (int)numUD_nDevices.Value);
        }

        private void button_stop_Click(object sender, EventArgs e)
        {
            button_start.Enabled = true;
            groupBox_capasity.Enabled = true;
            groupBox_outer.Enabled = true;
            groupBox_device.Enabled = true;
            button_stop.Enabled = false;
            mController.destroy();
        }
        
    }
}
