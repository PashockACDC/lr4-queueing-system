﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace LR4_Queueing_System.Model
{
    /// <summary>
    /// Обслуживающее устройство
    /// (работает по Гауссовому закону распределения)
    /// </summary>
    class ProcessingDevice
    {

        /// <summary>Очередь заявок</summary>
        private Queue<int> mQueue;
        /// <summary>среднее время обслуживания 1-ой заявки (сек)</summary>
        private double mTimeProcessingAverage;
        /// <summary>разброс (сек)</summary>
        private double mTimeProcessingScatter;
        /// <summary>минимальное время обработки заявки</summary>
        private double mTimeProcessingMin;
        /// <summary>номер обрабатываемой заявки</summary>
        private int mN = 0;
        /// <summary>длина очереди</summary>
        private int mQueueCapasity;
        /// <summary>количество уствройств</summary>
        private int mDevices;

        /// <summary>кол-во обрабатываемых заявок в данный момент</summary>
        private int mStatProcessing = 0;
        /// <summary>кол-во пропущенных заявок</summary>
        private int mStatFail = 0;
        /// <summary>кол-во занятых устройств</summary>
        private int mStatBusy = 0;

        /// <summary>поток, отслеживающий всю статистику</summary>
        private Thread threadStatistics;
        /// <summary>поток, обрабатывающий поступление внешних событий (заявок)</summary>
        private Thread threadCommonProcessing;
        /// <summary>пул потоков, обрабатывающих каждую заявку</summary>
        private Thread[] threadsProcessing;
        /// <summary>флаги - обрабатываются ли заявки потоками-обработчиками</summary>
        private bool[] isRunning;
        /// <summary>существует ли в данный момент обслуживающее устройство</summary>
        private bool isExist = false;

        /// <summary>событие - сообщаем статистику:
        /// <para>- кол-во пропущенных заявок</para>
        /// <para>- кол-во занятых устройств</para>
        /// <para>- всего устройств</para>
        /// <para>- всего заявок поступило</para>
        /// <para>- заявок в очереди</para>
        /// </summary>
        public event Action<int, int, int, int, int> onShowStatistic;
        /// <summary>событие - заявка поставлена в очередь</summary>
        public event Action<int> onPutRequestToProcessing;
        /// <summary>событие - заявка обработана</summary>
        public event Action<int> onRequestComplete;
        /// <summary>событие - заявка пропала</summary>
        public event Action<int> onRequestFail;


        /// <summary>
        /// Создание обслуживающего устройства
        /// с заданными параметрами
        /// </summary>
        /// <param name="timeProcessingAverage">среднее время обслуживания 1-ой заявки (сек)</param>
        /// <param name="timeProcessingScatter">разброс</param>
        /// <param name="queueCapasity">длина очереди</param>
        /// <param name="nDevices">кол-во обслуживающих аппаратов</param>
        /// <param name="timeProcessingMin">минимальное время обработки заявки</param>
        public ProcessingDevice(double timeProcessingAverage,
                                double timeProcessingScatter,
                                int queueCapasity,
                                int nDevices,
                                double timeProcessingMin = 0.1)
        {
            this.mTimeProcessingAverage = timeProcessingAverage;
            this.mTimeProcessingScatter = timeProcessingScatter;
            this.mQueueCapasity = queueCapasity > 0 ? queueCapasity : 1;
            this.mQueue = new Queue<int>(queueCapasity);
            this.mDevices = nDevices > 0 ? nDevices : 1;
            this.threadsProcessing = new Thread[mDevices];
            this.isRunning = new bool[mDevices];
            this.mTimeProcessingMin = timeProcessingMin > 0.1 ? timeProcessingMin : 0.1;
            this.isExist = true;
            this.threadCommonProcessing = new Thread(running);
            this.threadCommonProcessing.Start();
            this.threadStatistics = new Thread(processingStatistic);
            this.threadStatistics.Start();
        }


        /// <summary>
        /// Уничтожение обслуживающего устройства
        /// (новое создавать только через new !!!)
        /// </summary>
        public void onDestroy()
        {
            this.isExist = false;
        }


        /// <summary>
        /// Основной обрабатывающий поток
        /// </summary>
        private void running()
        {
            while (isExist)
            {
                    int N;
                if (mQueue.Count > 0)
                {
                    for (int i = 0; i < mDevices; i++)
                    {
                        if (!isRunning[i])
                        {
                            N = mQueue.Dequeue();
                            if (onPutRequestToProcessing != null)
                                onPutRequestToProcessing(N);
                            isRunning[i] = true;
                            threadsProcessing[i] = new Thread(() => { processingOneRequest(i, N); });
                            threadsProcessing[i].Start();
                            break;
                        }
                    }
                }
            }
        }


        /// <summary>
        /// Обработка одной заявки
        /// </summary>
        /// <param name="n">номер потока, который будет обрабатывать заявку</param>
        /// <param name="N">номер обрабатываемой заявки</param>
        private void processingOneRequest(int n, int N)
        {
            mStatBusy++;
            mStatProcessing++;
            NormalRandom nrand = new NormalRandom();
            double t = nrand.NextDouble() * mTimeProcessingScatter + mTimeProcessingAverage;
            t = t > mTimeProcessingMin ? t : mTimeProcessingMin;
            Thread.Sleep( (int)(t * 1000) > 0 ? (int)(t * 1000) : 0 );
            if (onRequestComplete != null)
                onRequestComplete(N);
            mStatBusy--;
            mStatProcessing--;
            isRunning[n] = false;
        }


        /// <summary>
        /// Генерация событий для статистики
        /// </summary>
        private void processingStatistic()
        {
            while (isExist)
            {
                if (onShowStatistic != null)
                    onShowStatistic(mStatFail, mStatBusy, mQueueCapasity, mN, mQueue.Count);
            }
        }


        /// <summary>
        /// Попытаться добавить заявку в обработку
        /// </summary>
        /// <returns>успех или не успех</returns>
        public bool tryAddRequest()
        {
            int n = ++mN;
            if (mQueue.Count < mQueueCapasity)
            {
                mQueue.Enqueue(n);
                return true;
            }
            else
            {
                if (onRequestFail != null)
                    onRequestFail(n);
                mStatFail++;
                return false;
            }
        }


    }
}
