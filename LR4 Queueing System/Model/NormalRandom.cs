﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LR4_Queueing_System.Model
{
    /// <summary>
    /// Класс для генерации случайных чисел с нормальным распределением.
    /// http://ru.stackoverflow.com/questions/491105/генерация-последовательностей-чисел
    /// 
    /// Этим классом можно пользоваться так же, как и Random:
    /// 
    /// NormalRandom nr = new NormalRandom();
    /// // ...
    /// double x = nr.NextDouble();
    /// 
    /// Если нужно получить нормальное распределение с другими μ и σ, применяйте скалирование:
    /// 
    /// double x = nr.NextDouble() * deviation + expectation;
    /// 
    /// </summary>
    public class NormalRandom : Random
    {
        /// <summary>сохранённое предыдущее значение</summary>
        double prevSample = double.NaN;

        protected override double Sample()
        {
            // есть предыдущее значение? возвращаем его
            if (!double.IsNaN(prevSample))
            {
                double result = prevSample;
                prevSample = double.NaN;
                return result;
            }

            // нет? вычисляем следующие два
            // Marsaglia polar method из википедии
            double u, v, s;
            do
            {
                u = 2 * base.Sample() - 1;
                v = 2 * base.Sample() - 1; // [-1, 1)
                s = u * u + v * v;
            }
            while (u <= -1 || v <= -1 || s >= 1 || s == 0);
            double r = Math.Sqrt(-2 * Math.Log(s) / s);

            prevSample = r * v;
            return r * u;
        }
    }
}
