﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace LR4_Queueing_System.Model
{
    /// <summary>
    /// Генератор внешних событий
    /// (по равномерному закону распределения)
    /// </summary>
    public class OuterGenerator
    {
        /// <summary>нижний предел (сек) времени поступления заявки</summary>
        private double minTime = 0.5;
        /// <summary>верхний предел (сек) времени поступления заявки</summary>
        private double maxTime = 1.5;

        /// <summary>поток, генерирующий внешние события (поступление заявки)</summary>
        private Thread threadOuterGenerator;
        /// <summary>генерируются ли события в данный момент</summary>
        private bool isGenerating = false;
        /// <summary>существует ли в данный момент внешний генератор</summary>
        private bool isExist = false;

        /// <summary>главное событие - поступила заявка</summary>
        public event Action<double> onGenerating;

        /// <summary>
        /// Создание внешнего генератора событий
        /// с заданными параметрами
        /// </summary>
        /// <param name="minT">нижний предел (сек) времени поступления заявки</param>
        /// <param name="maxT">верхний предел (сек) времени поступления заявки</param>
        public OuterGenerator(double minT, double maxT)
        {
            this.minTime = minT;
            this.maxTime = maxT;
            this.isExist = true;
            this.isGenerating = true;
            this.threadOuterGenerator = new Thread(running);
            this.threadOuterGenerator.Start();
        }

        /// <summary>
        /// Уничтожение генератора внешней среды
        /// (новый создавать только через new !!!)
        /// </summary>
        public void onDestroy()
        {
            this.isExist = false;
        }


        /// <summary>
        /// Основной обрабатывающий поток
        /// </summary>
        private void running()
        {
            while (isExist)
            {
                if (isGenerating)
                {
                    Random rand = new Random();
                    double t = minTime + rand.NextDouble() * (maxTime - minTime);
                    Thread.Sleep((int)(t * 1000));
                    if (onGenerating != null)
                        onGenerating(t);
                }
            }
        }

    }
}
