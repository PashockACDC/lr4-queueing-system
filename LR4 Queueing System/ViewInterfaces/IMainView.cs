﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LR4_Queueing_System.ViewInterfaces
{
    /// <summary>
    /// Соглашение о том, какие методы должно реализовать
    /// главное представление
    /// </summary>
    public interface IMainView
    {

        /// <summary>
        /// TEST TEST TEST TEST TEST TEST TEST
        /// Логирование
        /// </summary>
        /// <param name="message"></param>
        /// <param name="outer">если true, то outer</param>
        void addMessageToLog(string message, bool outer = true);

        /// <summary>
        /// Отображение статистики
        /// </summary>
        /// <param name="nFails">кол-во пропущенных заявок</param>
        /// <param name="nBusy">кол-во занятых устройств</param>
        /// <param name="nAllDevices">всего устройств</param>
        /// <param name="nAllReq">всего заявок поступило</param>
        /// <param name="nReqInQueue">заявок в очереди</param>
        void showStatictic(int nFails, int nBusy, int nAllDevices, int nAllReq, int nReqInQueue);
    }
}
