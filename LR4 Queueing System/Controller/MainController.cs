﻿using LR4_Queueing_System.Model;
using LR4_Queueing_System.ViewInterfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LR4_Queueing_System.Controller
{
    /// <summary>
    /// Главный контроллер
    /// </summary>
    public class MainController
    {
        private IMainView mForm;
        private OuterGenerator mOuter;
        private ProcessingDevice mDevice;

        public MainController(IMainView form)
        {
            this.mForm = form;
        }


        /// <summary>
        /// Создание системы массового обслуживания
        /// (внешний генератор + обслуживающее устройство)
        /// с заданными параметрами
        /// </summary>
        /// <param name="minT">нижний предел (сек) времени поступления заявки
        /// от внешнего генератора</param>
        /// <param name="maxT">верхний предел (сек) времени поступления заявки
        /// от внешнего генератора</param>
        /// <param name="procAverage">среднее время обслуживания 1-ой заявки (сек)</param>
        /// <param name="procScatter">разброс</param>
        /// <param name="queueCapasity">длина очереди</param>
        /// <param name="nDevices">количество обслуживающих устройств</param>
        public void createSystem(double minT, double maxT,
                                 double procAverage, double procScatter,
                                 int queueCapasity, int nDevices)
        {
            this.mOuter = new OuterGenerator(minT, maxT);
            this.mDevice = new ProcessingDevice(procAverage, procScatter, queueCapasity, nDevices);
            this.mOuter.onGenerating += mOuter_onGenerating;
            this.mDevice.onPutRequestToProcessing += mDevice_onPutRequestToProcessing;
            this.mDevice.onRequestComplete += mDevice_onRequestComplete;
            this.mDevice.onRequestFail += mDevice_onRequestFail;
            this.mDevice.onShowStatistic += mDevice_onShowStatistic;
        }


        public void destroy()
        {
            if (mOuter != null && mDevice != null)
            {
                this.mOuter.onDestroy();
                this.mDevice.onDestroy();
                this.mOuter.onGenerating -= mOuter_onGenerating;
                this.mDevice.onPutRequestToProcessing -= mDevice_onPutRequestToProcessing;
                this.mDevice.onRequestComplete -= mDevice_onRequestComplete;
                this.mDevice.onRequestFail -= mDevice_onRequestFail;
            }
        }


        private void mOuter_onGenerating(double t)
        {
            bool success = mDevice.tryAddRequest();
            mForm.addMessageToLog(t.ToString("F3") + " " + (success ? "+" : "-"));
        }

        private void mDevice_onPutRequestToProcessing(int t)
        {
            mForm.addMessageToLog("Обраб. заявка № " + t.ToString(), false);
        }

        private void mDevice_onRequestComplete(int n)
        {
            mForm.addMessageToLog("Готова заявка № " + n.ToString(), false);
        }

        private void mDevice_onRequestFail(int n)
        {
            mForm.addMessageToLog("Пропала заявка № " + n.ToString(), false);
        }

        /// <summary>
        /// Прокидывание статистики
        /// </summary>
        /// <param name="nFails">кол-во пропущенных заявок</param>
        /// <param name="nBusy">кол-во занятых устройств</param>
        /// <param name="nAllDevices">всего устройств</param>
        /// <param name="nAllReq">всего заявок поступило</param>
        /// <param name="nReqInQueue">заявок в очереди</param>
        private void mDevice_onShowStatistic(int nFails, int nBusy, int nAllDevices, int nAllReq, int nReqInQueue)
        {
            mForm.showStatictic(nFails, nBusy, nAllDevices, nAllReq, nReqInQueue);
        }
    }
}
